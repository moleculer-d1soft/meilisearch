export interface MeilisearchMixinOptions {
  meiliHost: string;
  meiliMasterKey: string;
}