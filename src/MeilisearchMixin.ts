import type { Service, ServiceSchema } from 'moleculer';
import { type Key, MeiliSearch } from 'meilisearch';
import type { MeilisearchMixinOptions } from './MeilisearchMixinOptions';

export class MeiliSearchMixin implements Partial<ServiceSchema<MeilisearchMixinOptions>>, ThisType<Service> {
  private schema?: Partial<ServiceSchema<MeilisearchMixinOptions>> & ThisType<Service>;

  protected meilisearch?: MeiliSearch;
  protected keys?: Key[];

  public async created(): Promise<void> {
    const host = this.schema?.settings?.meiliHost;
    const masterKey = this.schema?.settings?.meiliMasterKey;

    if (!host) {
      throw new Error(`Can't start search service, meiliHost is undefined`);
    }

    this.meilisearch = new MeiliSearch({
      host,
      apiKey: masterKey
    });

    this.keys = (await this.meilisearch.getKeys()).results;

    return Promise.resolve();
  }
}