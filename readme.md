# Moleculer d1soft / MeiliSearch

Meilisearch mixin for moleculer

## Install

```sh
npm add -E meilisearch @moleculer-d1soft/meilisearch
```

## Example

```ts
import { Service as MoleculerService, ServiceBroker, ServiceSettingSchema } from 'moleculer';
import { Service } from 'moleculer-decorators';
import { MeilisearchMixin, type MeilisearchMixinOptions, type MeiliSearch } from '@bukusaya/meilisearch';

interface SomeServiceSettings extends ServiceSettingSchema, MeilisearchMixinOptions {}

@Service({
    name: 'some-service',
    version: 1,
    settings: {
      meiliHost: process.env.MEILI_HOST || 'http://localhost:7700',
      meiliMasterKey: process.env.MEILI_MASTER_KEY || 'master',

      $secureSettings: ['meiliMasterKey']
    },
    mixins: [new MeilisearchMixin()]
})
export default class SomeService extends MoleculerService<SomeServiceSettings> {
  protected meili?: MeiliSearch;

	public constructor(public broker: ServiceBroker) {
		super(broker);
	}

  ...
}

```

## Links
- [MeiliSearch](https://docs.meilisearch.com/)